With this app, I am trying to demostrate some of my skills on Android develpment.

The app was implemented following M.V.C. structure.
you can easily find out from the code according to the package names.

Here are the points that I am attempting to demostrate in each part.

---

Model

I have defined 3 data models for the app.
Schedule: The basic model to presnet a schedule entity.  Also I added a static method "instanceFromJson" so that I can create instances from json.

UnionSchedule & IntersectionSchedule: these 2 are abstract classes. The purpose is to provide an interface for deferent requriments for displaying the intersection or union content. In this app, I provide a very simple implementation which is just to display a string. Plese see IntersectionActivity.StringIntersectionSchedule
&
UnionActivity.StringUnionSchedule
for details

---

Controllers


I defined an abstract class ScheduleManager. which will be extended to implement to manage the data with different stratigies for C.R.U.D. operations .
I have provide one implementation called PreferenceScheduleManagerImpl, which is use the local preference and json to store data.
Also, in this part, I was trying to demo factory patten and singleton pattern.

---

Views

The App uses only Activities for user interfaces.

schedule
MainActivity : display all/remove schedules,  schedules.
DetailActivity : display selected schedule.
CreateActivity : create new schedules.
UnionActivity.StringUnionSchedule : Calculate   either the   intersection or the union of two schedules, and return the resulting
(I am not satisfied with this part, so I will say this is incompleted. Please let me know if you want me to do more for it)

For each of the Activities, I was trying to demo the usages of some native SDK.

MainActivity:
- Implemented "View Holder" pattern for using ListView.  but actually, it is easier to use RecyclerView for practical projects because this class has already integrated this pattern.
- Demo how to pass parameters between Activities.
- Demo how to custom gesture with Views by implemented the UI of delete button.
-
DetailsActivity:
demo how to use one way data binding.

CreateActivity: demo how to use 2-way data binding

Please also check the demo.mp4 in the repo if you have difficulty to use the app.

Thank you.

Tony
