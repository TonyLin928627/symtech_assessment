package com.example.tonylin.interviewassessment.models;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.JsonObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tonylin on 3/10/17.
 */

public final class Schedule implements Serializable {

    private final static SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
    private final static SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
    private final static SimpleDateFormat sdfDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    private long id;
    private long startTimeStamp;
    private int duration;
    private String title, description;

    private boolean mon,tue,wed,thu,fri,sat,sun;

    private String relatedImage;

    public Schedule(long id) {
        this.id = id;

        this.title="";
        this.description="";
        this.startTimeStamp = System.currentTimeMillis()+(60*1000);
        this.duration = 1;
    }

    public long getId(){
        return this.id;
    }

    public long getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(long startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRelatedImage() {
        return relatedImage;
    }

    public void setRelatedImage(String relatedImage) {
        this.relatedImage = relatedImage;
    }

    public boolean isMon() {
        return mon;
    }

    public void setMon(boolean mon) {
        this.mon = mon;
    }

    public boolean isTue() {
        return tue;
    }

    public void setTue(boolean tue) {
        this.tue = tue;
    }

    public boolean isWed() {
        return wed;
    }

    public void setWed(boolean wed) {
        this.wed = wed;
    }

    public boolean isThu() {
        return thu;
    }

    public void setThu(boolean thu) {
        this.thu = thu;
    }

    public boolean isFri() {
        return fri;
    }

    public void setFri(boolean fri) {
        this.fri = fri;
    }

    public boolean isSat() {
        return sat;
    }

    public void setSat(boolean sat) {
        this.sat = sat;
    }

    public boolean isSun() {
        return sun;
    }

    public void setSun(boolean sun) {
        this.sun = sun;
    }

    public String getTimeBeginDateStr(){

        Date date = new Date(this.startTimeStamp);

        return sdfDate.format(date);
    }

    public String getTimeBeginTimeStr(){

        Date date = new Date(this.startTimeStamp);

        return sdfTime.format(date);
    }


    public void setStartTime(@NonNull String dateTimeStr) {

        try {
            Date dateTime = sdfDateTime.parse(dateTimeStr);
            this.setStartTimeStamp(dateTime.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public static Schedule instanceFromJson(JsonObject jsonObject) {

        long id = jsonObject.get("id").getAsLong();
        long startTimeStamp = jsonObject.get("startTimeStamp").getAsLong();
        int duration = jsonObject.get("duration").getAsInt();
        String title = jsonObject.get("title").getAsString();
        String description = jsonObject.get("description").getAsString();
        boolean sun = jsonObject.get("sun").getAsBoolean();
        boolean mon = jsonObject.get("mon").getAsBoolean();
        boolean tue = jsonObject.get("tue").getAsBoolean();
        boolean wed = jsonObject.get("wed").getAsBoolean();
        boolean thu = jsonObject.get("thu").getAsBoolean();
        boolean fri = jsonObject.get("fri").getAsBoolean();
        boolean sat = jsonObject.get("sat").getAsBoolean();

        Schedule schedule = new Schedule(id);
        schedule.setStartTimeStamp(startTimeStamp);
        schedule.setDuration(duration);
        schedule.setTitle(title);
        schedule.setDescription(description);
        schedule.setSun(sun);
        schedule.setMon(mon);
        schedule.setTue(tue);
        schedule.setWed(wed);
        schedule.setThu(thu);
        schedule.setFri(fri);
        schedule.setSat(sat);

        return schedule;
    }
}
