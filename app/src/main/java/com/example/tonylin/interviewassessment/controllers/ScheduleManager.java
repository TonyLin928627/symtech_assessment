package com.example.tonylin.interviewassessment.controllers;

import android.support.annotation.NonNull;

import com.example.tonylin.interviewassessment.models.IntersectionSchedule;
import com.example.tonylin.interviewassessment.models.Schedule;
import com.example.tonylin.interviewassessment.models.UnionSchedule;

import java.util.List;

/**
 * Created by tonylin on 3/10/17.
 */

public abstract class ScheduleManager{


    public enum ScheduleManagerType{
        PREFERENCE,
        SQLite,
        REST_API;
    }

    public abstract  List<Schedule> getAllSchedules();
    public abstract Schedule generateNewSchedule();
    public abstract void insertSchedule(Schedule schedule) throws ScheduleOperationFailException;
    public abstract void deleteSchedule(long id) throws ScheduleOperationFailException;
    public abstract void updateSchedule(Schedule schedule) throws ScheduleOperationFailException;
    public abstract IntersectionSchedule calculateIntersection(Schedule schedule1, Schedule schedule2) throws ScheduleOperationFailException;
    public abstract UnionSchedule calculateUnion(Schedule schedule1, Schedule schedule2) throws ScheduleOperationFailException;

    public static ScheduleManager createScheduleManager(@NonNull ScheduleManagerType type) throws UnsupportedOperationException{

        ScheduleManager scheduleManagerImpl = null;

        switch (type){
            case PREFERENCE:
                scheduleManagerImpl= PreferenceScheduleManagerImpl.getSingleton();
            break;

            case SQLite:
                throw new UnsupportedOperationException("SQLite ScheduleManager is not supported yet.");

            case REST_API:
                throw new UnsupportedOperationException("REST_API ScheduleManager is not supported yet.");
        }

        return scheduleManagerImpl;

    }
}
