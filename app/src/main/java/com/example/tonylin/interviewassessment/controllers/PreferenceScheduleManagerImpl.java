package com.example.tonylin.interviewassessment.controllers;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.tonylin.interviewassessment.App;
import com.example.tonylin.interviewassessment.models.IntersectionSchedule;
import com.example.tonylin.interviewassessment.models.Schedule;
import com.example.tonylin.interviewassessment.models.UnionSchedule;
import com.example.tonylin.interviewassessment.utils.Util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by tonylin on 3/10/17.
 */

public class PreferenceScheduleManagerImpl extends ScheduleManager{

    static final String TAG = "ScheduleManager";

    static String SCHEDULE_SET_STORE_KEY = "com.example.tonylin.interviewassessment.SCHEDULE_SET_STORE_KEY";
    private final static PreferenceScheduleManagerImpl instance = new PreferenceScheduleManagerImpl();
    public static PreferenceScheduleManagerImpl getSingleton(){
        return instance;
    }

    private SharedPreferences preference;
    private Semaphore semaphore = new Semaphore(1);

    private PreferenceScheduleManagerImpl(){
        super();

         this.preference = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    }

    @Override
    public List<Schedule> getAllSchedules() {

        this.requestAccessPermission();

        String jsonArrayStr= preference.getString(SCHEDULE_SET_STORE_KEY, "[]");

        final List<Schedule> returnList = Util.convertJsonArrayStringToScheduleList(jsonArrayStr);

        Collections.sort(returnList, new Comparator<Schedule>() {
            @Override
            public int compare(Schedule o1, Schedule o2) {

                return (o1.getId() - o2.getId())>0?1:-1;
            }
        });

        this.releaseAccessPermission();

        return returnList;
    }

    @Override
    public Schedule generateNewSchedule() {
        //use the current time as id
        return new Schedule(System.currentTimeMillis());

    }

    @Override
    public void insertSchedule(Schedule schedule)  throws ScheduleOperationFailException{

        this.requestAccessPermission();

        String jsonArrayStr = preference.getString(SCHEDULE_SET_STORE_KEY, "[]");

        List<Schedule> schedules = Util.convertJsonArrayStringToScheduleList(jsonArrayStr);
        schedules.add(schedule);

        jsonArrayStr = Util.convertScheduleListToJsonArrayString(schedules);

        boolean isSavedSuccessfully = preference.edit().putString(SCHEDULE_SET_STORE_KEY, jsonArrayStr).commit();

        this.releaseAccessPermission();

        if (!isSavedSuccessfully) {
            throw new ScheduleOperationFailException("Fail to insert schedule " + schedule);
        }else{
            Log.d(TAG, "inserted Schedule "+ schedule.getTitle());
        }

    }

    @Override
    public void deleteSchedule(long id) throws ScheduleOperationFailException{

        this.requestAccessPermission();

        String jsonArrayStr = preference.getString(SCHEDULE_SET_STORE_KEY, "[]");

        List<Schedule> schedules = Util.convertJsonArrayStringToScheduleList(jsonArrayStr);

        Integer indexToBeRemove = null;
        for (int i=0; i<schedules.size(); i++){
            if (id == schedules.get(i).getId()){
                indexToBeRemove = i;
                break;
            }
        }

        if (indexToBeRemove!=null){

            Schedule deletedSchedule  = schedules.remove(indexToBeRemove.intValue());
            if (deletedSchedule!=null) {

                jsonArrayStr = Util.convertScheduleListToJsonArrayString(schedules);
                boolean isSavedSuccessfully = preference.edit().putString(SCHEDULE_SET_STORE_KEY, jsonArrayStr).commit();

                if (!isSavedSuccessfully) {
                    throw new ScheduleOperationFailException("Fail to insert schedule with id" + id);
                } else {
                    Log.d(TAG, "deleted Schedule " + deletedSchedule.getTitle());
                }
            }

        }else{
            throw new ScheduleOperationFailException("Fail to insert schedule with id" + id);
        }

        this.releaseAccessPermission();
    }

    @Override
    public void updateSchedule(@NonNull Schedule schedule)  throws ScheduleOperationFailException{
        throw new ScheduleOperationFailException("updateSchedule has not been implemented yet");
    }

    @Override
    public IntersectionSchedule calculateIntersection(Schedule schedule1, Schedule schedule2) throws ScheduleOperationFailException{
        return null;
    }

    @Override
    public UnionSchedule calculateUnion(Schedule schedule1, Schedule schedule2) throws ScheduleOperationFailException {
        return null;
    }

    private void requestAccessPermission(){
        try {
            this.semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void releaseAccessPermission(){
        this.semaphore.release();
    }

//    private Schedule searchForScheduleWithId(long id, Set<String> stringSet){
//
//        for (String jsonStr : stringSet){
//            Schedule schedule = Util.convertJsonToSchedule(jsonStr);
//            if (schedule!=null && schedule.getId()==id){
//
//                return schedule;
//            }
//        }
//
//        return null;
//    }
}
