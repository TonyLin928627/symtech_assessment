package com.example.tonylin.interviewassessment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.SeekBar;
import android.widget.TimePicker;

import com.example.tonylin.interviewassessment.databinding.ActivityCreateBinding;
import com.example.tonylin.interviewassessment.controllers.ScheduleOperationFailException;
import com.example.tonylin.interviewassessment.models.Schedule;
import com.example.tonylin.interviewassessment.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class CreateActivity extends AppCompatActivity {

    private ActivityCreateBinding binding;
    private List<CheckBox> weekDayCheckboxes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_create);

        this.weekDayCheckboxes = new ArrayList<>();
        this.weekDayCheckboxes.add(binding.sunCb);
        this.weekDayCheckboxes.add(binding.monCb);
        this.weekDayCheckboxes.add(binding.thuCb);
        this.weekDayCheckboxes.add(binding.wedCb);
        this.weekDayCheckboxes.add(binding.thuCb);
        this.weekDayCheckboxes.add(binding.friCb);
        this.weekDayCheckboxes.add(binding.satCb);

        this.binding.durationSb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser){
                    Schedule schedule = binding.getNewSchedule();

                    schedule.setDuration(progress);

                    binding.setNewSchedule(schedule);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        this.binding.setPresenter(this);
        binding.setNewSchedule(App.getInstance().getScheduleManager().generateNewSchedule());

    }

    public void onPickDateClick(View v){

        String[] dateStrings = binding.dateTv.getText().toString().split("-");
        int dateInt = Integer.parseInt(dateStrings[0]);
        int monthInt = Integer.parseInt(dateStrings[1]);
        int yearInt = Integer.parseInt(dateStrings[2]);

        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.Theme_AppCompat_Light_Dialog,

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        binding.dateTv.setText(dayOfMonth+"-"+(month+1)+"-"+year);

                        setRepeatDay();
                    }
                }, yearInt, monthInt-1, dateInt);


        datePicker.show();
    }

    private void setRepeatDay() {

        Integer dayInWeek = Util.getDayInWeek(binding.dateTv.getText().toString());

        if (dayInWeek!=null) {
            this.weekDayCheckboxes.get(dayInWeek-1).setChecked(true);
        }
    }

    public void onPickTimeClick(View v){

        String[] timeStrings = binding.timeTv.getText().toString().split(":");
        int hourInt = Integer.parseInt(timeStrings[0]);
        int minuteInt = Integer.parseInt(timeStrings[1]);

        TimePickerDialog timePicker = new TimePickerDialog(this, R.style.Theme_AppCompat_Light_Dialog,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        binding.timeTv.setText(hourOfDay+":"+minute);

                    }
                }, hourInt, minuteInt, true);

        timePicker.show();
    }

    public void onCreateClick(View v){
        Schedule schedule = binding.getNewSchedule();
        schedule.setStartTime(binding.dateTv.getText()+" "+binding.timeTv.getText());


        String validateMsg;
        if ((validateMsg = validateNewSchedule()) == null) {

            try {

                App.getInstance().getScheduleManager().insertSchedule(schedule);

                this.setResult(Activity.RESULT_OK);

                this.finish();
            } catch (ScheduleOperationFailException e) {
                e.printStackTrace();
            }
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage(validateMsg).setCancelable(false).setNegativeButton("OK", null).show();
        }
    }

    private String validateNewSchedule() {

        if (TextUtils.isEmpty(binding.getNewSchedule().getTitle())){
            return getResources().getString(R.string.empty_title_err_msg);
        }

        if (binding.getNewSchedule().getStartTimeStamp() < System.currentTimeMillis()){
            return getResources().getString(R.string.invalid_start_time_msg);
        }

        return null;
    }
}
