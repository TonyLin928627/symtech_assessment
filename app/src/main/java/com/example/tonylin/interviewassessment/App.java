package com.example.tonylin.interviewassessment;

import android.app.Application;
import android.util.DisplayMetrics;

import com.example.tonylin.interviewassessment.controllers.ScheduleManager;

/**
 * Created by tonylin on 3/10/17.
 */

public class App extends Application {

    private static App app;

    public static App getInstance(){
        return app;
    }

    private ScheduleManager scheduleManager;

    public Float Density;

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;

        scheduleManager = ScheduleManager.createScheduleManager(ScheduleManager.ScheduleManagerType.PREFERENCE);

        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        this.Density = metrics.density;
    }

    public ScheduleManager getScheduleManager() {
        return scheduleManager;
    }
}
