package com.example.tonylin.interviewassessment;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.tonylin.interviewassessment.databinding.ActivityIntersectionBinding;
import com.example.tonylin.interviewassessment.databinding.ActivityUnionBinding;
import com.example.tonylin.interviewassessment.models.IntersectionSchedule;
import com.example.tonylin.interviewassessment.models.Schedule;
import com.example.tonylin.interviewassessment.models.UnionSchedule;

public class IntersectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Schedule firstSchedule = (Schedule)getIntent().getSerializableExtra(MainActivity.FIRST_SCHEDULE);
        Schedule secondSchedule = (Schedule)getIntent().getSerializableExtra(MainActivity.SECOND_SCHEDULE);

        ActivityIntersectionBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_intersection);

        binding.setIntersectionSchedule(new IntersectionActivity.StringIntersectionSchedule(firstSchedule, secondSchedule));
    }

    public class StringIntersectionSchedule extends IntersectionSchedule<String> {

        protected StringIntersectionSchedule(Schedule firstSchedule, Schedule secondSchedule) {
            super(firstSchedule, secondSchedule);
        }

        @Override
        public String getIntersectionForDisplay() {
            String s  = "this is the Intersection content for "+this.firstSchedule.getTitle() +" and "+this.secondSchedule.getTitle();

            return s;
        }


    }
}

