package com.example.tonylin.interviewassessment;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.tonylin.interviewassessment.databinding.ActivityUnionBinding;
import com.example.tonylin.interviewassessment.models.Schedule;
import com.example.tonylin.interviewassessment.models.UnionSchedule;

public class UnionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Schedule firstSchedule = (Schedule)getIntent().getSerializableExtra(MainActivity.FIRST_SCHEDULE);
        Schedule secondSchedule = (Schedule)getIntent().getSerializableExtra(MainActivity.SECOND_SCHEDULE);

        ActivityUnionBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_union);

        binding.setUnionSchedule(new StringUnionSchedule(firstSchedule, secondSchedule));
    }

    public class StringUnionSchedule extends UnionSchedule<String>{

        protected StringUnionSchedule(Schedule firstSchedule, Schedule secondSchedule) {
            super(firstSchedule, secondSchedule);
        }

        @Override
        public String getUnionForDisplay() {

            String s  = "this is the union content for "+this.firstSchedule.getTitle() +" and "+this.secondSchedule.getTitle();

            return s;
        }
    }
}
