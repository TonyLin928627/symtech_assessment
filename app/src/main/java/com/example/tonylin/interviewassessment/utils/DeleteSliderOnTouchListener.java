package com.example.tonylin.interviewassessment.utils;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.example.tonylin.interviewassessment.App;
import com.example.tonylin.interviewassessment.R;

/**
 * Created by tonylin on 5/10/17.
 */

public class DeleteSliderOnTouchListener implements View.OnTouchListener {

    private static final String TAG = "DeleteSlider";
    private long initTime;
    private final float DELETE_BTN_WIDTH = App.getInstance().getResources().getDimension(R.dimen.delete_btn_width);
    private static final long DEFAULT_DURATION = 150;
    private static final long CLICK_DURATION = 250;

    float initX;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float lastX;
        Log.d(TAG, "action: "+event.getAction());

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:

                this.initX = v.getX();
                Log.d(TAG, "initX: "+(initX/App.getInstance().Density));

                lastX = event.getRawX();
                v.setTag(lastX);

                initTime = System.currentTimeMillis();

                break;

            case MotionEvent.ACTION_MOVE:

                lastX =  (float)v.getTag();

                float deltaX = event.getRawX()-lastX;

                float newX = v.getX() + (deltaX);

                if (newX<-DELETE_BTN_WIDTH){
                    newX = -DELETE_BTN_WIDTH;
                }else if(newX>0){
                    newX = 0;
                }

                v.setX(newX);

                Log.d(TAG, "newX: "+(newX/App.getInstance().Density));

                lastX = event.getRawX();

                v.setTag(lastX);


                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:


                if ((System.currentTimeMillis() - initTime) <= CLICK_DURATION) {

                    float delta = (this.initX - v.getX());

                    if (delta>150){
                        playToggleFocusTimeDeleteButtonAnimation(v, DEFAULT_DURATION, v.getX(),  -DELETE_BTN_WIDTH);
                        Log.d(TAG, "endX: "+(v.getX()/App.getInstance().Density) +" swipe to open by "+ delta);

                        break;

                    }else if (delta<-150){
                        playToggleFocusTimeDeleteButtonAnimation(v, DEFAULT_DURATION, v.getX(), 0.0f);
                        Log.d(TAG, "endX: "+(v.getX()/App.getInstance().Density) +" swipe to close by "+ delta);

                        break;
                    }else if (v.getX()==-DELETE_BTN_WIDTH) {
                        playToggleFocusTimeDeleteButtonAnimation(v, DEFAULT_DURATION, v.getX(), 0.0f);
                        Log.d(TAG, "endX: "+(v.getX()/App.getInstance().Density) +"clickToClose");

                        break;
                    }else{
                        resetPosition(v);
                        Log.d(TAG, "endX: "+(v.getX()/App.getInstance().Density) +" resetPosition");

                        break;
                    }

                }else{
                    resetPosition(v);
                    Log.d(TAG, "endX: "+(v.getX()/App.getInstance().Density) +" resetPosition");

                    break;
                }

        }


        return true;
    }

    private void resetPosition(View focusTimeItemView) {

        float currentX = focusTimeItemView.getX();

        long duration = (long)(DEFAULT_DURATION * (currentX/-DELETE_BTN_WIDTH));

        Log.d(TAG, "resetPosition: "+ (currentX/App.getInstance().Density) +" in "+duration);

        if (currentX >= -DELETE_BTN_WIDTH/2){
            //slide to right to close
            playToggleFocusTimeDeleteButtonAnimation(focusTimeItemView, duration, currentX, 0.0f);
        }else{
            //slide to left to open
            playToggleFocusTimeDeleteButtonAnimation(focusTimeItemView, duration, currentX, -DELETE_BTN_WIDTH);
        }

    }

    private static void playToggleFocusTimeDeleteButtonAnimation(final View v, long duration, float fromX, float toX) {

        if (v==null){
            return;
        }

        ValueAnimator animator = ValueAnimator.ofFloat(fromX, toX);
        animator.setDuration(duration);
        animator.setRepeatCount(0);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float updatedValue = (Float) animation.getAnimatedValue();

                v.setX(updatedValue);

            }
        });

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                v.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                v.setEnabled(true);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                v.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();

    }

}
