package com.example.tonylin.interviewassessment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.tonylin.interviewassessment.controllers.ScheduleOperationFailException;
import com.example.tonylin.interviewassessment.models.Schedule;
import com.example.tonylin.interviewassessment.utils.DeleteSliderOnTouchListener;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_CREATE = 9001;
    public static final String SCHEDULE = "SCHEDULE";
    public static final String FIRST_SCHEDULE = "FIRST_SCHEDULE";
    public static final String SECOND_SCHEDULE = "SECOND_SCHEDULE";

    private ListView schedulesLv;
    private List<Schedule> allSchedules;

    private Set<Schedule> selectedScheduleSet = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toCreateActivity();
            }
        });

        this.schedulesLv = (ListView)this.findViewById(R.id.schedules_lv);

        displayAllSchedules();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode==REQUEST_CODE_CREATE && resultCode==Activity.RESULT_OK){
            displayAllSchedules();
        }
    }

    private void toCreateActivity() {
        Intent i = new Intent(this, CreateActivity.class);
        this.startActivityForResult(i, REQUEST_CODE_CREATE);
    }

    private void toDetailActivity(Schedule schedule) {

        Intent i = new Intent(this, DetailsActivity.class);

        i.putExtra(SCHEDULE, schedule);

        this.startActivity(i);
    }

    private void removeSelectedSchedule(Schedule schedule) {
        this.selectedScheduleSet.remove(schedule);
    }

    private void putSelectedSchedule(Schedule schedule) {

        this.selectedScheduleSet.add(schedule);

        if (selectedScheduleSet.size()==2){
            popIntersectionOrUnionDialog();
        }
    }

    private void popIntersectionOrUnionDialog() {

        Object[] objects = selectedScheduleSet.toArray();
        final Schedule[] selectedSchedules = new Schedule[]{(Schedule)objects[0], (Schedule)objects[1]};
        selectedScheduleSet.clear();

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.intersection_union_short))
                .setItems(new CharSequence[]{"Display Intersection Schedule", "Display Union Schedule"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                displayAllSchedules();
                                toIntersectionActivity(selectedSchedules);
                                break;
                            case 1:
                                displayAllSchedules();
                                toUnionActivity(selectedSchedules);
                                break;
                        }
                    }
                })

                .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        displayAllSchedules();
                    }
                }).show();
    }

    private void toIntersectionActivity(Schedule[] selectedSchedules) {

        Intent i = new Intent(this, IntersectionActivity.class);

        i.putExtra(FIRST_SCHEDULE, selectedSchedules[0]);
        i.putExtra(SECOND_SCHEDULE, selectedSchedules[1]);

        this.startActivity(i);
    }

    private void toUnionActivity(Schedule[] selectedSchedules){
        Intent i = new Intent(this, UnionActivity.class);

        i.putExtra(FIRST_SCHEDULE, selectedSchedules[0]);
        i.putExtra(SECOND_SCHEDULE, selectedSchedules[1]);

        this.startActivity(i);
    }

    private void popDeleteScheduleDialog(final long id) {
        new AlertDialog.Builder(this).setMessage(R.string.delete_confirm_msg)
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            App.getInstance().getScheduleManager().deleteSchedule(id);

                            displayAllSchedules();
                        } catch (ScheduleOperationFailException e) {
                            e.printStackTrace();
                        }
                    }
                }).show();
    }

    private void displayAllSchedules(){
        this.allSchedules = App.getInstance().getScheduleManager().getAllSchedules();
        this.schedulesLv.setAdapter(new SchedulesAdapter());
    }

    private class SchedulesAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return allSchedules.size();
        }

        @Override
        public Object getItem(int position) {
            return allSchedules.get(position);
        }

        @Override
        public long getItemId(int position) {
            return allSchedules.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v==null){
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                v = inflater.inflate(R.layout.view_schedule_item, parent, false);

                TextView titleTv = (TextView)v.findViewById(R.id.title_tv);
                ImageView detailsIv = (ImageView)v.findViewById(R.id.details_iv);
                CheckBox selectedCb = (CheckBox)v.findViewById(R.id.selected_cb);
                TextView deleteTv = (TextView)v.findViewById(R.id.delete_tv);
                View sliderV = v.findViewById(R.id.slider_rl);

                ViewHolder vh = new ViewHolder(titleTv, selectedCb, detailsIv, deleteTv,sliderV);

                v.setTag(vh);
            }

            ViewHolder vh = (ViewHolder)v.getTag();

            vh.setSchedule(allSchedules.get(position));

            return v;
        }

        private class ViewHolder implements View.OnClickListener {
            private TextView titleTv;
            private CheckBox selectedCb;
            private ImageView detailsIv;
            private TextView deleteTv;
            private View sliderV;

            private Schedule schedule;

            private ViewHolder(@NonNull TextView titleTv , CheckBox selectedCb, ImageView detailsIv, TextView deleteTv, View sliderV){
                this.titleTv = titleTv;
                this.selectedCb = selectedCb;
                this.detailsIv = detailsIv;
                this.deleteTv = deleteTv;
                this.sliderV = sliderV;

                this.detailsIv.setOnClickListener(this);
                this.selectedCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            putSelectedSchedule(schedule);
                        }else{
                            removeSelectedSchedule(schedule);
                        }
                    }
                });
                this.deleteTv.setOnClickListener(this);
                this.sliderV.setOnTouchListener(new DeleteSliderOnTouchListener());

            }

            public void setSchedule(Schedule schedule) {
                this.schedule = schedule;

                this.titleTv.setText(this.schedule.getTitle());

            }

            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.details_iv:
                        toDetailActivity(this.schedule);
                        break;
                    case R.id.delete_tv:
                        popDeleteScheduleDialog(this.schedule.getId());
                }
            }
        }
    }

}
