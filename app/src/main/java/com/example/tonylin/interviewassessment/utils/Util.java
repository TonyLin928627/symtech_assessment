package com.example.tonylin.interviewassessment.utils;

import android.content.Intent;

import com.example.tonylin.interviewassessment.models.Schedule;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by tonylin on 3/10/17.
 */

public class Util {

//    public static String convertScheduleToJson(Schedule schedule){
//
//        Gson gson = new Gson();
//        Type type = new TypeToken<Schedule>(){}.getType();
//
//        String jsonStr = gson.toJson(schedule, type);
//
//        return jsonStr;
//
//    }

    public static List<Schedule> convertJsonArrayStringToScheduleList(String jsonStr) {

        List<Schedule> list = new ArrayList<>();
        try {
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(jsonStr).getAsJsonArray();

            for (int i=0; i<jsonArray.size(); i++){

                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                list.add(Schedule.instanceFromJson(jsonObject));
            }

        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }finally {
            return list;
        }
    }

    public static String convertScheduleListToJsonArrayString(List<Schedule> schedules) {


        String jsonArrayStr = new Gson().toJson(schedules);


        return jsonArrayStr;

    }

    public static Integer getDayInWeek(String dateStr) {

        Integer dayInWeek = null;
        final SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date = sdfDate.parse(dateStr);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            dayInWeek = calendar.get(Calendar.DAY_OF_WEEK);

        } catch (ParseException e) {
            e.printStackTrace();

        }finally {
            return dayInWeek;
        }
    }
}
