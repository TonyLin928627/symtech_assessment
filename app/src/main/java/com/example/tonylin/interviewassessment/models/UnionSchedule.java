package com.example.tonylin.interviewassessment.models;

/**
 * Created by tonylin on 3/10/17.
 */

public abstract class UnionSchedule<T> {

    protected Schedule firstSchedule, secondSchedule;
    protected UnionSchedule(Schedule firstSchedule, Schedule secondSchedule){
        this.firstSchedule = firstSchedule;
        this.secondSchedule = secondSchedule;
    }
    public abstract T getUnionForDisplay();
}
