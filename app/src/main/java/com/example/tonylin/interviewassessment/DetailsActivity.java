package com.example.tonylin.interviewassessment;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.tonylin.interviewassessment.databinding.ActivityDetailsBinding;
import com.example.tonylin.interviewassessment.models.Schedule;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ActivityDetailsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        binding.setPresenter(this);

        Schedule schedule = (Schedule)getIntent().getSerializableExtra(MainActivity.SCHEDULE);
        binding.setSchedule(schedule);
    }

    public void onOkClick(View v){
        this.finish();
    }
}
